use std::{
    path::PathBuf,
    sync::{mpsc::Sender, Arc, Mutex},
    time::{Duration, Instant},
};

use eframe::{
    egui::{self, RichText, SidePanel, Widget},
    epi::{self, Frame},
};

use crate::steam::{find_steam_paths, Action, Game};

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Settings {
    pub action: Action,
    pub timer: Duration,
    pub refresh: Duration,
    pub notify: bool,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            action: Action::Shutdown,
            timer: Duration::from_secs(30),
            refresh: Duration::from_secs(3),
            notify: true,
        }
    }
}

#[derive(Debug)]
pub struct Control {
    pub settings: Settings,

    pub libraryfolders: PathBuf,
    pub games: Vec<Game>,
    pub last_update: Instant,

    pub running: bool,
    pub was_downloading: bool,

    pub timer_remaining: Option<Duration>,
}

impl Control {
    pub fn new() -> Self {
        let steam_path = find_steam_paths()
            .into_iter()
            .find(|path| path.join("steamapps").join("libraryfolders.vdf").exists())
            .expect("Could not find steam installation folder");

        let libraryfolders = steam_path.join("steamapps").join("libraryfolders.vdf");

        let settings = Settings::default();

        Self {
            libraryfolders,
            games: Vec::new(),
            last_update: std::time::Instant::now()
                .checked_sub(settings.refresh)
                .expect("invalid refresh value"),
            running: false,
            was_downloading: false,
            timer_remaining: None,
            settings,
        }
    }
}

#[derive(Debug, Clone)]
pub struct App {
    timer_text: String,
    refresh_text: String,
    control: Arc<Mutex<Control>>,
    tx: Option<Sender<Frame>>,
}

impl App {
    pub fn new(control: Arc<Mutex<Control>>, tx: Sender<Frame>) -> Self {
        let (timer_text, refresh_text) = {
            let lock = control.lock().unwrap();

            (
                lock.settings.timer.as_secs().to_string(),
                lock.settings.refresh.as_secs().to_string(),
            )
        };

        Self {
            timer_text,
            refresh_text,
            control,
            tx: Some(tx),
        }
    }
}

impl epi::App for App {
    fn name(&self) -> &str {
        "Shutdown After Steam Downloads"
    }

    /// Called once before the first frame.
    fn setup(&mut self, _ctx: &egui::CtxRef, frame: &Frame, storage: Option<&dyn epi::Storage>) {
        if let Some(storage) = storage {
            let settings: Settings = epi::get_value(storage, epi::APP_KEY).unwrap_or_default();

            self.timer_text = settings.timer.as_secs().to_string();
            self.refresh_text = settings.refresh.as_secs().to_string();

            self.control.lock().unwrap().settings = settings;
        }

        self.tx.take().unwrap().send(frame.clone()).unwrap();
    }

    fn save(&mut self, storage: &mut dyn epi::Storage) {
        epi::set_value(
            storage,
            epi::APP_KEY,
            &self.control.lock().unwrap().settings,
        );
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &epi::Frame) {
        let Self {
            timer_text,
            refresh_text,
            control,
            ..
        } = self;

        let mut lock = control.lock().unwrap();

        let Control {
            settings:
                Settings {
                    action,
                    refresh,
                    timer,
                    notify,
                },

            games,

            running,
            was_downloading,
            timer_remaining,
            ..
        } = &mut *lock;

        SidePanel::new(egui::panel::Side::Right, "right_panel").show(ctx, |ui| {
            egui::Grid::new("game_grid").num_columns(2).show(ui, |ui| {
                ui.label("Refresh Interval");
                if ui.text_edit_singleline(refresh_text).lost_focus() {
                    if let Ok(new_timer) = refresh_text.parse() {
                        *refresh = Duration::from_secs(new_timer);
                    } else {
                        *refresh_text = refresh.as_secs().to_string();
                    }
                }
                ui.end_row();

                ui.label("Shutdown Timer");
                if ui.text_edit_singleline(timer_text).lost_focus() {
                    if let Ok(new_timer) = timer_text.parse() {
                        *timer = Duration::from_secs(new_timer);
                    } else {
                        *timer_text = timer.as_secs().to_string();
                    }
                }
                ui.end_row();
            });

            ui.checkbox(notify, "Notify before Shutdown");

            ui.horizontal(|ui| {
                ui.radio_value(action, Action::Shutdown, format!("{:?}", Action::Shutdown));
                ui.radio_value(action, Action::Reboot, format!("{:?}", Action::Reboot));
                ui.radio_value(action, Action::Exit, format!("{:?}", Action::Exit));
            });

            ui.add_space(10.);

            if timer_remaining.is_some() {
                if ui.button("Cancel").clicked() {
                    *timer_remaining = None;
                    *running = false;
                    *was_downloading = false;
                }
            } else {
                #[allow(clippy::collapsible_else_if)]
                if *running {
                    if ui
                        .button(if games.is_empty() {
                            "Stop Waiting for Downloads"
                        } else {
                            "Stop"
                        })
                        .clicked()
                    {
                        *running = false;
                        *was_downloading = !games.is_empty();
                    }
                } else {
                    if ui
                        .button(if games.is_empty() {
                            "Wait for Downloads"
                        } else {
                            "Start"
                        })
                        .clicked()
                    {
                        *running = true;
                        *was_downloading = !games.is_empty();
                    }
                }
            }
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            if let Some(remaining) = timer_remaining {
                ui.centered_and_justified(|ui| {
                    ui.label(format!("Will {:?} in {:?}", *action, *remaining));
                });
            } else {
                egui::Grid::new("game_grid").num_columns(2).show(ui, |ui| {
                    for game in games.iter() {
                        let mut name = RichText::new(&game.name);

                        if game.state_flags & 0b10000000000 != 0 {
                            name = name.strong();
                        }

                        if game.state_flags & 0b100 != 0 {
                            name = name.underline()
                        }

                        ui.label(name);

                        // ui.label(format!("Flags: {:b}", game.state_flags));

                        egui::widgets::ProgressBar::new(if game.download_size_known() {
                            game.bytes_staged as f32 / game.bytes_to_stage as f32
                        } else {
                            0.
                        })
                        .show_percentage()
                        .animate(!game.download_size_known())
                        .ui(ui);

                        ui.end_row();
                    }
                });
            }
        });
    }
}

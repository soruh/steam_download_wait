// #![windows_subsystem = "windows"]

use std::{
    sync::{mpsc::channel, Arc, Mutex},
    thread::{sleep, spawn},
    time::Instant,
};

use eframe::epi::IconData;
use ui::Control;
use worker::update;

mod steam;
mod ui;
mod worker;

const ICON_DATA: &[u8] = include_bytes!(concat!(env!("OUT_DIR"), "/icon.rgba"));

fn main() {
    let steam_path = steam::find_steam_paths()
        .into_iter()
        .find(|path| path.join("steamapps").join("libraryfolders.vdf").exists())
        .expect("Could not find steam installation folder");

    let libraryfolders =
        steam::find_libraries(&steam_path.join("steamapps").join("libraryfolders.vdf"));

    let mut games = steam::read_games(&libraryfolders).collect::<Vec<_>>();
    games.sort_unstable_by(|a, b| a.state_flags.cmp(&b.state_flags).then(a.name.cmp(&b.name)));

    for game in games {
        let flags = format!("{:016b}", game.state_flags)
            .chars()
            .enumerate()
            .map(|(i, c)| {
                let color = if c == '1' {
                    match 15 - i {
                        1 => "\x1b[33m",
                        2 => "\x1b[32m",
                        9 => "\x1b[31m",
                        10 => "\x1b[34m",
                        _ => "",
                    }
                } else {
                    ""
                };

                #[cfg(target_os = "windows")]
                {
                    c.to_string()
                }

                #[cfg(not(target_os = "windows"))]
                {
                    format!("{}{}\x1b[m", color, c)
                }
            })
            .collect::<String>();

        println!(
            "{} | {} | {} | {:>11}/{:<11} | {:>11}/{:<11} | {:6.2}% | {:6.2}% | {:7} | {}",
            flags,
            if let Some(res) = game.update_result {
                format!("{res:2}")
            } else {
                "??".to_owned()
            },
            game.scheduled_auto_update
                .map(|x| x.to_string())
                .unwrap_or_else(|| "-".repeat("2022-03-19T16:19:30".len())),
            game.bytes_downloaded,
            game.bytes_to_download,
            game.bytes_staged,
            game.bytes_to_stage,
            if game.bytes_to_download != 0 {
                game.bytes_downloaded as f64 / game.bytes_to_download as f64 * 100.
            } else {
                100.
            },
            if game.bytes_to_stage != 0 {
                game.bytes_staged as f64 / game.bytes_to_stage as f64 * 100.
            } else {
                100.
            },
            game.appid,
            game.name,
        )
    }

    // return;

    let control = Arc::new(Mutex::new(Control::new()));

    let (tx, rx) = channel();

    let app = ui::App::new(control.clone(), tx);

    let _ = spawn(move || {
        let frame = rx.recv().unwrap();
        loop {
            let start = Instant::now();
            let next_update_in = update(&mut *control.lock().unwrap());

            sleep(next_update_in.saturating_sub(start.elapsed()));
            frame.request_repaint();
        }
    });

    eframe::run_native(
        Box::new(app),
        eframe::NativeOptions {
            icon_data: Some(IconData {
                width: u32::from_le_bytes(ICON_DATA[..4].try_into().unwrap()),
                height: u32::from_le_bytes(ICON_DATA[4..8].try_into().unwrap()),
                rgba: ICON_DATA[8..].to_owned(),
            }),
            ..Default::default()
        },
    );
}

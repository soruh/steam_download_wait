#![allow(dead_code, unused)]

use crate::ui::App;

use std::{
    collections::HashMap,
    convert::Infallible,
    fmt::Display,
    num::ParseIntError,
    path::{Path, PathBuf},
    process::exit,
    str::FromStr,
    thread::{sleep, spawn},
    time::{Duration, Instant},
};

use chrono::NaiveDateTime;
use system_shutdown::{reboot, shutdown};

pub fn find_steam_paths() -> Vec<PathBuf> {
    #[cfg(target_os = "linux")]
    {
        vec![PathBuf::from(format!(
            "{}/.local/share/Steam",
            std::env::var("HOME").unwrap()
        ))]
    }

    #[cfg(target_os = "windows")]
    vec![
        PathBuf::from("C:\\Program Files (x86)\\Steam"),
        PathBuf::from("C:\\Program Files\\Steam"),
    ]
}

#[derive(Debug)]
pub struct Library {
    pub path: PathBuf,
    pub games: HashMap<String, usize>,
}

pub fn find_libraries(libraryfolders: &Path) -> Vec<Library> {
    let mut content = vdf::from_file(libraryfolders, "libraryfolders").unwrap();

    (0_usize..)
        .map_while(|x| content.remove(&x.to_string()))
        .map(|value| {
            let mut value = value.as_dict().unwrap();
            Library {
                path: PathBuf::from(value.remove("path").unwrap().as_string().unwrap()),
                games: value
                    .remove("apps")
                    .unwrap()
                    .as_dict()
                    .unwrap()
                    .into_iter()
                    .map(|(key, value)| (key, value.as_string().unwrap().parse().unwrap()))
                    .collect(),
            }
        })
        .collect()
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Game {
    pub name: String,
    pub installdir: String,
    pub state_flags: u16,
    pub size_on_disk: u64,
    pub appid: u64,
    pub bytes_downloaded: u64,
    pub bytes_to_stage: u64,
    pub bytes_to_download: u64,
    pub bytes_staged: u64,
    pub appmanifest_path: PathBuf,
    pub update_result: Option<u16>,
    pub scheduled_auto_update: Option<NaiveDateTime>,
}

macro_rules! field {
    ($map: expr, $name: literal) => {
        $map.remove($name)
            .expect(concat!("Game data is missing the '", $name, "' field"))
            .as_string()
            .expect(concat!(
                "Game data field '",
                $name,
                "' had an unexpected format"
            ))
            .parse()
            .expect(concat!(
                "Game data field '",
                $name,
                "' had an unexpected format"
            ))
    };
    (optional, $map: expr, $name: literal) => {
        $map.contains_key($name).then(|| field!($map, $name))
    };
}

impl Game {
    fn new(mut map: HashMap<String, vdf::Value>, steamapps: &Path) -> Self {
        let appid = field!(map, "appid");

        let scheduled_auto_update: i64 = field!(map, "ScheduledAutoUpdate");

        let scheduled_auto_update = (scheduled_auto_update != 0)
            .then(|| chrono::NaiveDateTime::from_timestamp(scheduled_auto_update, 0));

        Self {
            name: field!(map, "name"),
            installdir: field!(map, "installdir"),
            state_flags: field!(map, "StateFlags"),
            size_on_disk: field!(map, "SizeOnDisk"),
            appid,
            bytes_downloaded: field!(optional, map, "BytesDownloaded").unwrap_or(0),
            bytes_to_stage: field!(optional, map, "BytesToStage").unwrap_or(0),
            bytes_to_download: field!(optional, map, "BytesToDownload").unwrap_or(0),
            bytes_staged: field!(optional, map, "BytesStaged").unwrap_or(0),
            update_result: field!(optional, map, "UpdateResult"),
            scheduled_auto_update,
            appmanifest_path: steamapps.join(format!("appmanifest_{}.acf", appid)),
        }

        // println!("ingnored values: {:#?}", map);
    }

    pub fn download_size_known(&self) -> bool {
        self.bytes_downloaded != 0 && self.bytes_to_stage != 0
    }

    pub fn finished(&self) -> bool {
        self.download_size_known()
            && self.bytes_downloaded == self.bytes_to_download
            && self.bytes_staged == self.bytes_to_stage
    }

    pub fn update_required(&self) -> bool {
        self.state_flags & 0b10 != 0
    }

    pub fn is_installed(&self) -> bool {
        self.state_flags & 0b100 != 0
    }

    pub fn unscheduled(&self) -> bool {
        self.state_flags & 0b10_0000_0000 != 0
    }

    pub fn update_started(&self) -> bool {
        self.state_flags & 0b100_0000_0000 != 0
    }
}

pub fn read_downloads(libraries: &[Library]) -> impl Iterator<Item = Game> + '_ {
    read_games(libraries).filter(|game| {
        game.update_required() && !game.unscheduled() && game.scheduled_auto_update.is_none()
    })
}

pub fn read_games(libraries: &[Library]) -> impl Iterator<Item = Game> + '_ {
    libraries.iter().flat_map(|library| {
        let steamapps = library.path.join("steamapps");

        steamapps
            .read_dir()
            .expect("Failed to access library dir")
            .filter_map(move |dir_entry| {
                let path = dir_entry.unwrap().path();

                let filename = path.file_name().unwrap().to_str().unwrap();

                use std::io::Read;

                if filename.starts_with("appmanifest_") && filename.ends_with(".acf") {
                    let content =
                        std::fs::read_to_string(&path).expect("Failed to open appmanifest");

                    if content.trim_start().is_empty() {
                        return None;
                    }

                    let content = vdf::from_string(&content, "AppState");

                    match content {
                        Err(err) if err.kind() == std::io::ErrorKind::NotFound => None,
                        Err(err) => {
                            println!("Failed to open {:?}: {}", path, err);
                            None
                        }
                        Ok(content) => Some(Game::new(content, &steamapps)),
                    }
                } else {
                    None
                }
            })
    })
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum Action {
    /// Will reboot the machine
    Reboot,
    /// Will shutdown the machine
    Shutdown,
    /// Will do nothing and just exit
    Exit,
}

impl Action {
    pub fn perform(&self) {
        match self {
            Action::Reboot => reboot().unwrap(),
            Action::Shutdown => shutdown().unwrap(),
            Action::Exit => {}
        }

        exit(0)
    }
}

pub fn get_downloads(libraryfolders: &Path) -> Vec<Game> {
    let libraries = find_libraries(libraryfolders);

    let mut games = read_downloads(&libraries).collect::<Vec<_>>();
    games.sort_by_key(|game| game.name.clone());
    games
}

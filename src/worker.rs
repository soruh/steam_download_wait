use std::time::{Duration, Instant};

use crate::{
    steam::get_downloads,
    ui::{Control, Settings},
};

pub fn update(control: &mut Control) -> Duration {
    let Control {
        settings:
            Settings {
                action,
                refresh,
                timer,
                notify,
            },
        games,
        libraryfolders,
        last_update,
        running,
        was_downloading,
        timer_remaining,
    } = control;

    if last_update.elapsed() >= *refresh {
        *last_update = Instant::now();
        *games = get_downloads(libraryfolders);

        if let Some(remaining) = timer_remaining {
            if games.is_empty() {
                *remaining = remaining.saturating_sub(*refresh);

                if remaining.is_zero() {
                    action.perform();
                }
            } else if *running {
                *timer_remaining = None;
            }
        } else if *running {
            if games.is_empty() {
                if *was_downloading {
                    *timer_remaining = Some(*timer);

                    if *notify {
                        let mut notification = notify_rust::Notification::new();

                        notification
                            .summary("Steam downloads finished")
                            .body(&format!("Will {:?} in {:?}.", action, timer));

                        #[cfg(not(target_os = "windows"))]
                        notification.hint(notify_rust::Hint::Resident(true));

                        let _ = notification.timeout(0).show().unwrap();
                    }
                }
            } else {
                *was_downloading = true;
            }
        }
    }

    *refresh
}

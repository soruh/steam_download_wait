# Steam Download Wait

Allows shutting / rebooting down your PC after all steam downloads have finished.

To perform some other action after downloads have finished you can run the program from a terminal and select the `Exit` action. The command will then exit after all downloads have finished. 
